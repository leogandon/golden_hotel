## <div align="center"> 👑 👑 GOLDEN PALACE 👑 👑 </div>


### <div align="center">**Bienvenue sur le dépôt de ReadingSheep !**<br/> <br/></div>
#### <div align="center">Ici sont regroupés les dossiers concernant notre application Org'olden Maid©, ainsi que les documents qui y sont associés. <br/> <br/>

### Sommaire : <br/> <br/>

[Logo de l'hôtel](https://gitlab.com/leogandon/golden_hotel/-/blob/main/IMAGES/logo.png) <br/> <br/>
[Cahier des Charges](https://gitlab.com/leogandon/golden_hotel/-/blob/main/CDC/CAHIER_DES_CHARGES.pdf) <br/> <br/>
[Diagramme de cas d'utilisation](https://gitlab.com/leogandon/golden_hotel/-/tree/main/DIAGRAMMES/cas_utilisation.png) <br/> <br/>
[Diagramme de classes (UML)](https;//gitlab.com/leogandon/golden_hotel/-/tree/main/DIAGRAMMES/UML.png) <br/> <br/>
[Code](https://gitlab.com/leogandon/golden_hotel/-/tree/main/CODE) <br/> <br/>


###### Logo by Léo GANDON & Jeremy MOUTON, (crown by [FreePik](https://www.freepik.com "Freepik")) </div>
